using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NumberWizard : MonoBehaviour
{
    [SerializeField] int max;
    [SerializeField] int min;
    [SerializeField] TextMeshProUGUI guessText;
    [SerializeField] TextMeshProUGUI guessCountText;
    [SerializeField] TextMeshProUGUI highLowText;
    int guess;
    int guessCount = 0;
    // Start is called before the first frame update
    void Start(){
        StartGame();
    }

    void StartGame()
    {
        max++;
        NextGuess();
        guessCountText.text = guessCount.ToString();
    }

    public void OnPressHigher()
    {
        min = guess;
        NextGuess();
        UpdateGuessCount();
    }

    public void OnPressLower()
    {
        max = guess;
        NextGuess();
        UpdateGuessCount();
    }

    void NextGuess()
    {
        guess = (max + min) / 2;
        guessText.text = guess.ToString();
    }

    public void UpdateGuessCount()
    {
        guessCount++;
        guessCountText.text = guessCount.ToString();
        ShowHighLowDialogue();
    }

    public void ShowHighLowDialogue()
    {
        if (guessCount == 1)
        {
            highLowText.text= "How close was I Is your number higher or lower";
        }
    }
}
